class PapersController < ApplicationController
  before_action :find_paper, only: [:show, :update, :edit, :destroy]
  before_action :authenticate_professor!, only: [:new, :edit]
  # before_action :authenticate_coordinator!, only: [:show]
  # before_action :authenticate_reviewer!, only: [:edit]
  layout 'professor'

  def show
  end

  def new
    @paper = Paper.new
  end

  def create  
    @paper = Paper.new(paper_params)
    @paper.professor_id = current_professor.id
    @paper.status = 'SUBMETIDO'   
    
    if @paper.save
      Event.create(log: 'Submetido para revisão', paper_id: @paper.id)
      redirect_to professor_index_path, notice: "Artigo cadastrado com sucesso."
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @paper.update(paper_params)
      redirect_to professor_index_path, notice: "Artigo editado com sucesso."
    else
      render 'edit'
    end
  end

  def destroy
    if @paper.destroy
      redirect_to professor_index_path, notice: 'Artigo apagado com sucesso.'
    else
      redirect_to papers_path(@paper), notice: 'Não foi possível apagar o Artigo.'
    end
  end

  private
  
  def find_paper
    @paper = Paper.find(params[:id])
  end

  def paper_params
    params.require(:paper).permit(:title, :original_file_key, :reviewed_file_key, :status, :professor_id, :reviewer_id, :coordinator_id)
  end
end
