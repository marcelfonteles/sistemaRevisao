module ApplicationHelper
  def paper_status(status)
    if status == 'SUBMETIDO'
      @status = 'Submetido para revisão'
    elsif status == 'RECEBIDO'
      @status  = 'Recebido pelo coordenador'
    elsif status == 'REVISOR'
      @status = 'Enviado para revisor'
    elsif status == 'DEVOLVIDO'
      @status = 'Devolvido para o coordenador'
    elsif status == 'VOLTOU REVISOR'
      @status = 'O Artigo voltou para o revisor'  
    elsif status == 'CONCLUIDA'
      @status = 'Revisão concluída'
    end
  end  
end
