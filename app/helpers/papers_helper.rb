module PapersHelper
  def paper_status_options
    @status_options = [['Submetido para revisão', 'SUBMETIDO'],['Recebido pelo coordenador', 'RECEBIDO'], ['Enviado para revisor', 'REVISOR'], ['Devolvido para o coordenador', 'DEVOLVIDO'], ['Revisão concluída', 'CONCLUIDA']]
  end
end
