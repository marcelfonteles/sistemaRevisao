module ReviewersHelper
  def paper_reviewer_status(status)
    if status == 'SUBMETIDO'
      @status = 'Submetido para revisão'
    end
    if status == 'RECEBIDO'
      @status  = 'Recebido pelo coordenador'
    end
    if status == 'REVISOR'
      @status = 'Enviado para você'
    end
    if status == 'DEVOLVIDO'
      @status = 'Devolvido para o coordenador'
    end
    if status == 'VOLTOU REVISOR'
      @status = 'O Artigo voltou para você'  
    end
    if status == 'CONCLUIDA'
      @status = 'Revisão concluída'
    end
    return @status
  end 
end
