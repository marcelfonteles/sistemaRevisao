# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)




Professor.create(name:'Johh Doe', phone:'9999-9999', academic_unit:'CT', email:'professor@email.com', password:'fonteles')
Professor.create(name:'Gilberto Freire', phone:'9999-9999', academic_unit:'CT', email:'professor2@email.com', password:'fonteles')
Professor.create(name:'João José', phone:'9999-9999', academic_unit:'CT', email:'professor3@email.com', password:'fonteles')


Coordinator.create(name:'Coordenador', email:'coordenador@email.com', password:'fonteles')
Coordinator.create(name:'Coordenador2', email:'coordenador2@email.com', password:'fonteles')



Reviewer.create(name:'Revisor', email:'revisor@email.com',password:'fonteles')
Reviewer.create(name:'Revisor2', email:'revisor2@email.com',password:'fonteles')
Reviewer.create(name:'Revisor3', email:'revisor3@email.com',password:'fonteles')
Reviewer.create(name:'Revisor4', email:'revisor4@email.com',password:'fonteles')





## Coordinators
Coordinator.create(name: 'Diana Costa Fortier Silva', cpf:'576.670.643-00', email: 'fortier.diana@gmail.com', password:'fonteles')
Coordinator.create(name: 'Márcia de Melo Fernandes Gradvohl',cpf:'153.748.743-49', email:'mfgradvohl@hotmail.com', password:'fonteles')


## Reviewers
Reviewer.create(name:'Larissa Teixeira da Costa', cpf:'044.473.123-79', email:'larissatcosta@alu.ufc.br', password:'fonteles')
Reviewer.create(name:'Albert Cristian Dutra da Mota', cpf:'067.424.583-05', email:'albertcristian13@gmail.com', password:'fonteles')
Reviewer.create(name:'Karla Yanara Barbosa Simião', cpf:'016.356.753-08', email:'karlayanara@hotmail.com', password:'fonteles')
Reviewer.create(name:'Letícia Mota e Mota', cpf:'020.599.213-76', email:'Leticiamota2@gmail.com', password:'fonteles')
Reviewer.create(name:'José Antonio Sousa Santos', cpf:'031.360.263-81', email:'tonisans1914@gmail.com', password:'fonteles')
Reviewer.create(name:'Yasmim Santos Rodrigues', cpf:'053.933.913-07', email:'yasmimcdz@gmail.com', password:'fonteles')
Reviewer.create(name:'Eurico Mayer Vaz', cpf:'027.988.203-32', email:'eurico.mayer@gmail.com', password:'fonteles')
Reviewer.create(name:'Karoline Zilah Santos Carneiro', cpf:'058.979.274-13', email:'karoline_zilah@yahoo.com.br', password:'fonteles')
Reviewer.create(name:'Lisandra Sousa da Costa', cpf:'662.844.493-72', email:'linna.dacosta@hotmail.com', password:'fonteles')
Reviewer.create(name:'Letícia Freitas de Assis', cpf:'059.007.083-57', email:'leticiaassis23@gmail.com', password:'fonteles')
Reviewer.create(name:'Naiane Araújo de Oliveira', cpf:'048.782.363.02', email:'naiiane.araujo@gmail.com', password:'fonteles')
Reviewer.create(name:'Luana Mara Loiola Macedo', cpf:'016.364.313-05', email:'luanamara@gmail.com', password:'fonteles')
Reviewer.create(name:'Juliana Lopes Gurgel', cpf:'028.422.083-31', email:'julianalgurgel@gmail.com', password:'fonteles')
Reviewer.create(name:'Francisco Arago Santiago Angelo', cpf:'045.680.563-06', email:'aragosantiago25@gmail.com', password:'fonteles')
Reviewer.create(name:'Laila Cavalcante Romualdo', cpf:'034.983.653-12', email:'lailacavalcante20@gmail.com', password:'fonteles')
Reviewer.create(name:'Matheus Lima Mendes', cpf:'607.545.153.62', email:'limamatheus0445@gmail.com', password:'fonteles')
Reviewer.create(name:'Iakob Lourenço Mota', cpf:'671.771.073-04', email:'iakoblm@gmail.com', password:'fonteles')
Reviewer.create(name:'Jaime José de Vasconcelos Neto', cpf:'047.847.923-92', email:'jaimevasconcelos@hotmail.com', password:'fonteles')
Reviewer.create(name:'Luiz Vicente Costa da Silva', cpf:'013.826.513-50', email:'luizvcs@gmail.com', password:'fonteles')
Reviewer.create(name:'Johnson Carlos Albuquerque', cpf:'600.436.943-81', email:'johnsonkcarlos@hotmail.com', password:'fonteles')
Reviewer.create(name:'Arthur Paiva Pereira Honório', cpf:'035.673.533-81', email:'arthur.teacher.12@gmail.com', password:'fonteles')
